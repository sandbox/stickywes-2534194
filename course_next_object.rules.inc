<?php
/**
 * @file
 * Some new Course rule actions.
 */

/**
 * Implements hook_rules_action_info().
 */
function course_next_object_rules_action_info() {
  return array(
    'course_next_object_fetch_next_object_url' => array(
      'label' => t('Get the next object URL in the course outline.'),
      'group' => t('Course'),
      'parameter' => array(
        'coid' => array(
          'label' => t('Course Object ID'),
          'type' => 'integer',
        ),
      ),
      'provides' => array(
        'course_url_fetched' => array('type' => 'text', 'label' => t('Fetched Course object URL.')),
      ),
    ),
  );
}

/**
 * Rule action: Fetch the URL of the next Course object.
 */
function course_next_object_fetch_next_object_url($coid) {
  $co = course_get_course_object_by_id($coid);
  $url = $co->getCourse()->getNext()->getURL();
  return array('course_url_fetched' => $url);
}
